import React from 'react';
import styled from 'styled-components';

const LoaderContainer = styled.div`
  display: block;
  font-size: 16px;
`;

const LoadingPage = () => (
  <section className="loadingPage">
    <LoaderContainer>Loading</LoaderContainer>
  </section>
);

export default LoadingPage;
