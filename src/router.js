import React from 'react';

const HomePage = React.lazy(() => import('./pages/homepage.jsx'));
const ErrorPage = React.lazy(() => import('./pages/errorpage.jsx'));

const routes = [
  {
    path: '/',
    exact: true,
    component: HomePage,
  },
  {
    path: '/error',
    component: ErrorPage,
  },
];

export default routes;
