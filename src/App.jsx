import React, { Suspense } from 'react';
import { Switch, BrowserRouter, Redirect, Route } from 'react-router-dom';
import router from './router';
import LoadingPage from './components/LoadingPage';

const App = () => {
  return (
    <div className="App">
      <Suspense fallback={<LoadingPage />}>
        <BrowserRouter>
          <Switch>
            {router.map((routeConfig) => {
              const { path } = routeConfig;
              return <Route {...routeConfig} key={path} />;
            })}
            <Redirect to="/error" />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
};

export default App;
